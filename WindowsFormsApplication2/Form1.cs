﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
       

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text.Length<6)
                    throw new System.ArgumentException("Ошибка", "");
                String result = "";
                DateTime dt = dateTimePicker1.Value;
                result = Convert.ToString(dt.DayOfYear);
                result += textBox1.Text.Substring(0, 6);
                int min = (dt.TimeOfDay.Hours * 60 + dt.TimeOfDay.Minutes) / 10;
                result += Convert.ToString(min);
                maskedTextBox1.Text = result;
            }
            catch (Exception e2){
                toolTip1.ToolTipTitle = "Ошибка";
                toolTip1.Show("Проверьте данные", this, textBox1.Location, 1000);
                return;
            }
            
           


        }

        private void button2_Click(object sender, EventArgs e)
        {
            String code = maskedTextBox1.Text;
            if (!((code.Length>=9)&&(code.Length<=12))){
                toolTip1.ToolTipTitle = "Ошибка";
                toolTip1.Show("Код поврежден", this, maskedTextBox1.Location, 5000);
                return;
            }
            int date = Convert.ToInt32(code.Substring(0, 3));
            String index = code.Substring(3, 6);
            int time = Convert.ToInt32(code.Substring(9, code.Length-9));

                String url = String.Format("http://gdeposylka.ru/info/pochtamt/{0}", index);
                System.Net.WebRequest reqGET = System.Net.WebRequest.Create(@url);
                System.Net.WebResponse resp = reqGET.GetResponse();
                System.IO.Stream stream = resp.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(stream);
                string s = sr.ReadToEnd();
                //  var fromEncodind = Encoding.GetEncoding(1251);//из какой кодировки
                //   var bytes = fromEncodind.GetBytes(s);
                //  var toEncoding = Encoding.UTF8;//в какую кодировку
                //  s = toEncoding.GetString(bytes);
                if (s.IndexOf("poct-cont") != -1)
                {
                    s = s.Substring(s.IndexOf("poct-cont") + 11, 300);
                    textBox1.Text = index+" , "+s.Substring(0, s.IndexOf("<br>"));
                }
                else
                {
                    toolTip1.ToolTipTitle = "Ошибка";
                    toolTip1.Show("Индекс неверен", this, maskedTextBox1.Location, 5000);
                }


                DateTime d1 = new DateTime(2013, 1, 1, 0, 0, 0); 
       
                dateTimePicker1.Value =   d1.AddDays(date-1).AddMinutes(time*10);
             
                
                

           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            maskedTextBox1.Text = "";
        }
    }
}
